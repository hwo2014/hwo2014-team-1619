package kitt.communication;

import java.util.Collection;
import java.util.HashMap;

import kitt.race.Car;
import kitt.race.Track;
import kitt.race.car.CarPosition;

public interface RaceDataReceiver {
	
	public void receivedGameJoin( String name, String key );
	
	public void receivedGameStart();
	
	public void receivedOwnCar( String name, String color );
	
	public void receivedGameInit( Track track, Collection<Car> cars, int laps, int maxLapTimeMs, boolean quickRace, int durationMs  );
	
	public void receivedCarPositions( HashMap<String, CarPosition> positions, int tick ); //positions contains map of car color to new position
	
	public void receivedCrash( String name, String color, int tick );
	
	public void receivedSpawn( String name, String color, int tick );
	
	public void receivedFinished( String gameId, String name, String color );
	
	public void receivedTournamentEnd();
	
	public void receivedGameEnd();
	
	public void receivedLapFinished(String color, int lapTime, int tick);
	
	public void receivedTurboAvailable( double turboDurationMilliseconds, int turboDurationTicks, double turboFactor );
	
	public void receivedTurboStart( String name, String color, int tick );
	
	public void receivedTurboEnd( String name, String color, int tick );
	
	public void receivedDNF( String name, String color, String reason, int tick );
	
}
