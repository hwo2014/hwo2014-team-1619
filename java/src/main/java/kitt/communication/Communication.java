package kitt.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import kitt.Bot;
import kitt.race.track.Segment;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

public class Communication {
	
	private Socket m_Socket = null;
	private PrintWriter m_Writer = null;
	private BufferedReader m_Reader = null;

	private Gson m_Gson;
	
	private HashSet<RaceDataReceiver> m_Receiver = null;

	public Communication() {
		m_Gson = new Gson();
		
		m_Receiver = new HashSet<RaceDataReceiver>();
	}

	public void addDataReceiver(RaceDataReceiver receiver) {
		m_Receiver.add(receiver);
	}
	
	public void removeDataReceiver(RaceDataReceiver receiver) {
		m_Receiver.remove(receiver);
	}
	
	public boolean connect(String host, int port ) {

		System.out.println("Connecting to " + host + ":" + port);

		try {
			m_Socket = new Socket(host, port);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}



		try {
			m_Writer = new PrintWriter(new OutputStreamWriter(m_Socket.getOutputStream(), "utf-8"));
			m_Reader = new BufferedReader(new InputStreamReader(m_Socket.getInputStream(), "utf-8"));
		} catch (IOException e) {
			e.printStackTrace();

			return disconnect();
		}
		
		System.out.println("..connected");
		return true;  
	}
	
	public boolean join( Bot bot ) {
		System.out.println("Joining race as " + bot.name());
		sendMessage( new JoinMessage(bot.name(), bot.key()) );
		return true;
	}
	
	public boolean joinRace( Bot bot, String trackName, int carCount, String password ) {
		System.out.println("Joining race as " + bot.name());
		sendMessage( new JoinRaceMessage(bot.name(), bot.key(), trackName, carCount, password) );
		return true;
	}
	
	public boolean createRace( Bot bot, String trackName, int carCount, String password ) {
		System.out.println("Creating race as " + bot.name());
		sendMessage( new CreateRaceMessage(bot.name(), bot.key(), trackName, carCount, password) );
		return true;
	}

	public boolean disconnect() {
		try {
			m_Socket.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		m_Socket = null;
		m_Writer = null;
		m_Reader = null;

		return true;
	}

	private kitt.race.Track buildTrack(GameInitMessage gameInitMessage) {
		ArrayList<Segment> segments = new ArrayList<Segment>();
		
		double[] laneOffsets = new double[gameInitMessage.data.race.track.lanes.size()];
		
		for( GameInitMessage.Lane lane : gameInitMessage.data.race.track.lanes ) {
			laneOffsets[lane.index] = lane.distanceFromCenter;
		}
		
		int id = 0;
		for( GameInitMessage.Piece piece : gameInitMessage.data.race.track.pieces ) {
		
			Segment segment;
			
			if( piece.length < 0.0 ) {
				segment = new kitt.race.track.CurveSegment(id++,  piece.bridge, piece.angle, laneOffsets, piece.radius, piece.sw);
			} else {
				segment = new kitt.race.track.StraightSegment(id++, piece.length, piece.bridge, laneOffsets, piece.sw);
			}
			
			segments.add( segment );
		}
		
		for( int i = 1; i < segments.size(); ++i) {
			segments.get(i-1).setNext(segments.get(i));
			segments.get(i).setPrev(segments.get(i-1));
		}
		
		if( segments.size() > 0 ) {
			segments.get(0).setPrev(segments.get(segments.size() - 1));
			segments.get(segments.size() - 1).setNext(segments.get(0));
		}
		
		kitt.race.Track track = new kitt.race.Track(segments);
		return track;
	}
	
	private ArrayList<kitt.race.Car> buildCarList( GameInitMessage gameInitMessage, kitt.race.Track track ) {
		ArrayList<kitt.race.Car> cars = new ArrayList<kitt.race.Car>();
		
		for( GameInitMessage.Car car : gameInitMessage.data.race.cars ) {
			kitt.race.Car c = new kitt.race.Car(car.id.name, car.id.color, car.dimensions.length, car.dimensions.width, car.dimensions.guideFlagPosition, track);
			cars.add(c);
		}
		
		return cars;
	}
	
	private HashMap<String, kitt.race.car.CarPosition> buildPositionMap(CarPositionMessage carPositionMessage) {
		HashMap<String, kitt.race.car.CarPosition> positions = new HashMap<String, kitt.race.car.CarPosition>();
		
		for( CarPositionMessage.Data data : carPositionMessage.data ) {
			kitt.race.car.CarPosition pos = new kitt.race.car.CarPosition(carPositionMessage.gameTick, data.angle, data.piecePosition.inPieceDistance,data.piecePosition.pieceIndex,data.piecePosition.lap,data.piecePosition.lane.startLaneIndex,data.piecePosition.lane.endLaneIndex);
			positions.put(data.id.color, pos);
		}

		return positions;
	}

	public boolean receiveData() throws JsonSyntaxException, IOException {
		String line = m_Reader.readLine();

		if(line != null) {
			
			final Message message = m_Gson.fromJson(line, Message.class);
			
			if (message.msgType.equals("carPositions")) {
				
				CarPositionMessage carPositionMessage = m_Gson.fromJson(line, CarPositionMessage.class);
				//System.out.println("Received: " + carPositionMessage.msgType);
				
				HashMap<String, kitt.race.car.CarPosition> positions = buildPositionMap(carPositionMessage);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedCarPositions(positions, carPositionMessage.gameTick);
				}
				
			} else if (message.msgType.equals("join")) {
						
				JoinMessage joinMessage = m_Gson.fromJson(line, JoinMessage.class);
				System.out.println("Received: " + joinMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedGameJoin(joinMessage.data.name, joinMessage.data.key);
				}
						
			} else if (message.msgType.equals("yourCar")) {
				
				YourCarMessage yourCarMessage = m_Gson.fromJson(line, YourCarMessage.class);
				System.out.println("Received: " + yourCarMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedOwnCar(yourCarMessage.data.name, yourCarMessage.data.color);
				}
				
			} else if (message.msgType.equals("gameInit")) {
				
				GameInitMessage gameInitMessage = m_Gson.fromJson(line, GameInitMessage.class);
				System.out.println("Received: " + gameInitMessage.msgType);
				
				kitt.race.Track track = buildTrack(gameInitMessage);
				ArrayList<kitt.race.Car> cars = buildCarList( gameInitMessage, track );
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedGameInit(track, cars, gameInitMessage.data.race.raceSession.laps, gameInitMessage.data.race.raceSession.maxLapTimeMs, gameInitMessage.data.race.raceSession.quickRace, gameInitMessage.data.race.raceSession.durationMs );
				}
				
			} else if (message.msgType.equals("gameEnd")) {
				
				GameEndMessage gameEndMessage = m_Gson.fromJson(line, GameEndMessage.class);
				System.out.println("Received: " + gameEndMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedGameEnd();
				}
				
			} else if (message.msgType.equals("tournamentEnd")) {
				
				System.out.println("Received: " + message.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedTournamentEnd();
				}
				
			} else if (message.msgType.equals("gameStart")) {
				
				System.out.println("Received: " + message.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedGameStart();
				}
				
			} else if (message.msgType.equals("crash")) {
				
				CrashMessage crashMessage = m_Gson.fromJson(line, CrashMessage.class);
				System.out.println("Received: " + crashMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedCrash(crashMessage.data.name, crashMessage.data.color, crashMessage.gameTick);
				}
				
			} else if (message.msgType.equals("spawn")) {
				
				SpawnMessage spawnMessage = m_Gson.fromJson(line, SpawnMessage.class);
				System.out.println("Received: " + spawnMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedSpawn(spawnMessage.data.name, spawnMessage.data.color, spawnMessage.gameTick);
				}
				
			} else if (message.msgType.equals("lapFinished")) {
				
				LapFinishedMessage lapFinishedMessage = m_Gson.fromJson(line, LapFinishedMessage.class);
				System.out.println("Received: " + lapFinishedMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedLapFinished(lapFinishedMessage.data.car.color,lapFinishedMessage.data.lapTime.millis, lapFinishedMessage.gameTick);
				}
				
			} else if (message.msgType.equals("dnf")) {
				
				DNFMessage dnfMessage = m_Gson.fromJson(line, DNFMessage.class);
				System.out.println("Received: " + dnfMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedDNF(dnfMessage.data.car.name, dnfMessage.data.car.color, dnfMessage.data.reason, dnfMessage.gameTick);
				}
				
			} else if (message.msgType.equals("finish")) {
				
				FinishMessage finishMessage = m_Gson.fromJson(line, FinishMessage.class);
				System.out.println("Received: " + finishMessage.msgType);
				
			} else if (message.msgType.equals("turboAvailable")) {
				
				TurboAvailableMessage turboAvailableMessage = m_Gson.fromJson(line, TurboAvailableMessage.class);
				System.out.println("Received: " + turboAvailableMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedTurboAvailable(turboAvailableMessage.data.turboDurationMilliseconds, turboAvailableMessage.data.turboDurationTicks, turboAvailableMessage.data.turboFactor);
				}
				
			}   else if (message.msgType.equals("turboStart")) {
				
				TurboStartMessage turboStartMessage = m_Gson.fromJson(line, TurboStartMessage.class);
				System.out.println("Received: " + turboStartMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedTurboStart(turboStartMessage.data.name, turboStartMessage.data.color, turboStartMessage.gameTick);
				}
				
			}   else if (message.msgType.equals("turboEnd")) {
				
				TurboEndMessage turboStartMessage = m_Gson.fromJson(line, TurboEndMessage.class);
				System.out.println("Received: " + turboStartMessage.msgType);
				
				for( RaceDataReceiver receiver : m_Receiver ) {
					receiver.receivedTurboEnd(turboStartMessage.data.name, turboStartMessage.data.color, turboStartMessage.gameTick);
				}
				
			}  else {
				
				System.out.println("Received: " + message.msgType);
				
			}
		}
		return true;

	}
	
	public void sendPing(int tick) {
		System.out.println("Sending Ping on tick "+tick);
		sendMessage(new PingMessage(tick));
	}
	
	public void sendThrottle(double throttle, int tick) {
		sendMessage(new ThrottleMessage(throttle,tick));
	}
	
	public void sendSwitch(String position, int tick) {
		sendMessage(new SwitchLaneMessage(position,tick));
	}
	
	public void sendTurbo(String turboMessage) {
		sendMessage( new TurboMessage(turboMessage) );
	}

	private void sendMessage(final Message message) {
		m_Writer.println(m_Gson.toJson(message));
		m_Writer.flush();
	}
	
	//---------- Message types ----------
	
	private static class Message {
		public String msgType;
	}
	
	private class PingMessage extends Message {
		int gameTick;
		protected PingMessage(int tick) {
			msgType = "ping";
			gameTick = tick;
		}
	}
	
	private static class ThrottleMessage extends Message {
		
		public ThrottleMessage(double throttle, int tick) {
			msgType = "throttle";
			data = throttle;
			gameTick = tick;
		}
		
		public double data;
		public int gameTick;
	}
	
	private static class SwitchLaneMessage extends Message {
		
		public SwitchLaneMessage(String direction, int tick) {
			msgType = "switchLane";
			data = direction;
			gameTick = tick;
		}
		
		public String data;
		public int gameTick;
	}
	
	private class TurboMessage extends Message {
		public String data;
		protected TurboMessage(String turboMessage) {
			msgType = "turbo";
			data = turboMessage;
		}
	}
	
	private static class JoinMessage extends Message {
		public BotId data;
		
		public JoinMessage() {
		}
		
		public JoinMessage(final String name, final String key) {
			msgType = "join";
			data = new BotId(name, key);
		}
		
	}
	
	private static class JoinRaceMessage extends Message {
		public Data data;
		
		public JoinRaceMessage(final String name, final String key, final String trackName, int carCount, final String password) {
			msgType = "joinRace";
			data = new Data();
			data.botId = new BotId(name, key);
			data.trackName = trackName;
			data.password = name;
			data.carCount = carCount;
		}
		
		public static class Data {
			public BotId botId;
			public String trackName;
			public String password;
			public int carCount;
		}
	}
	
	private static class CreateRaceMessage extends Message {
		public Data data;
		
		public CreateRaceMessage(final String name, final String key, final String trackName, int carCount, final String password) {
			msgType = "createRace";
			data = new Data();
			data.botId = new BotId(name, key);
			data.trackName = trackName;
			data.password = name;
			data.carCount = carCount;
		}
		
		public static class Data {
			public BotId botId;
			public String trackName;
			public String password;
			public int carCount;
		}
	}
	
	private static class YourCarMessage extends Message {
		public ID data;
	}
	
	private static class GameInitMessage extends Message {
		public Data data;
		
		public static class Data {
			public Race race;
		}
		
		public static class Race {
			public Track track;
			public Collection<Car> cars;
			public RaceSession raceSession;
		}
		
		public static class Track {
			public String id;
			public String name;
			public Collection<Piece> pieces;
			public Collection<Lane> lanes;
			public Start startingPoint;
		}
		
		public static class Piece {
			public double length = -1.0;
			public double radius = 0.0;
			public double angle = 0.0;
			@SerializedName("switch") public boolean sw = false;
			public boolean bridge = false;
		}
		
		public static class Lane {
			int distanceFromCenter;
			int index;
		}
		
		public static class Start {
			public Position position;
			public double angle;
		}
		
		public static class Car {
			public ID id;
			public Dimension dimensions;
		}
		
		public static class Dimension {
			public double length;
			public double width;
			public double guideFlagPosition;
		}
		
		public static class RaceSession {
			public int laps = 0;
			public int maxLapTimeMs = 0;
			public boolean quickRace = false;
			public int durationMs = 0;
		}
		
	}
	
	private static class GameEndMessage extends Message {
		public Data data;
		
		public static class Data {
			Collection<CarResult> results;
			Collection<CarResult> bestLaps;
		}
		
		public static class CarResult {
			public ID car;
			public Result result;
		}
		
	}
	
	private static class GameMessage extends Message {
		public int gameTick;
	}
	
	
	private static class TurboAvailableMessage extends GameMessage {
		public Data data;
		
		public static class Data {
			public double turboDurationMilliseconds;
			public int turboDurationTicks;
			public double turboFactor;
		}
		
	}
	
	private static class TurboStartMessage extends GameMessage {
		public ID data;
	}
	
	private static class TurboEndMessage extends GameMessage {
		public ID data;
	}
	
	private static class CarPositionMessage extends GameMessage {
		public Collection<Data> data;
		
		public static class Data {
			public ID id;
			public double angle;
			public PiecePosition piecePosition;
		}
		
		public static class PiecePosition {
			public int pieceIndex;
			public double inPieceDistance;
			public LaneIndicies lane;
			public int lap;
		}
		
		public static class LaneIndicies {
			public int startLaneIndex;
			public int endLaneIndex;
		}
	}
	
	private static class CrashMessage extends GameMessage {
		public ID data;
	}
	
	private static class SpawnMessage extends GameMessage {
		public ID data;
	}
	
	private static class LapFinishedMessage extends GameMessage {
		public Data data;
		
		public static class Data {
			public ID car;
			public Result lapTime;
			public Result raceTime;
			public Ranking ranking;
		}
		
		public static class Ranking {
			public int overall;
			public int fastestLap;
		}
	}
	
	private static class DNFMessage extends GameMessage {
		public Data data;
		
		public static class Data {
			public ID car;
			public String reason;
		}
	}
	
	private static class FinishMessage extends GameMessage {
		public ID data;
	}
	
	private static class Position {
		public double x;
		public double y;
	}
	
	private static class ID {
		public String name;
		public String color;
	}
	
	private static class BotId {
		public BotId() {
			
		}
		
		public BotId(String name, String key) {
			this.name = name;
			this.key = key;
		}
		
		public String name;
		public String key;
	}
	
	public static class Result {
		public int laps = 0;
		public int ticks = 0;
		public int millis = 0;
	}

}
