package kitt;

import kitt.race.Car;
import kitt.race.Race;
import kitt.race.car.CarPosition;
import kitt.race.track.Segment;
import kitt.race.track.Segment.SegmentType;

public abstract class Bot {
	
	public static final String LEFT = "Left";
	public static final String RIGHT = "Right";
	public static final String STRAIGHT = "Straight";
	
	private String m_BotName;
	private String m_BotKey;
	
	private String m_Color;
	private Race m_Race = null;
	private Car m_Car = null;
	
	protected double m_LastSpeed = -1.0;
	protected double m_MaxSpeed = 0;
	
	public Bot(String botName, String botKey, Race race) {
		m_BotName = botName;
		m_BotKey = botKey;
		m_Race = race;
	}
	
	public String name() {
		return m_BotName;
	}
	
	public String key() {
		return m_BotKey;
	}
	
	public void setColor( String color ) {
		m_Color = color;
	}
	
	public String color() {
		return m_Color;
	}
	
	public void setCar( Car car ) {
		m_Car = car;
	}
	
	public Car car() {
		return m_Car;
	}
	
	public Race race() {
		return m_Race;
	}
	
	public void setThrottle(double throttle, int tick) {
		m_Race.communication().sendThrottle(throttle, tick);
	}
	
	public void switchLane( String lane, int tick ) {
		if( lane.equals(LEFT) || lane.equals(RIGHT) ) {
			m_Race.communication().sendSwitch(lane, tick);
		}
	}
	
	public void requestTurbo(String turboMessage) {
		race().requestTurbo(turboMessage);
	}
	
	public void sendPing(int tick) {
		m_Race.communication().sendPing(tick);
	}
	
	public Segment currentSegment() {
		return race().track().getSegment(car().position().segmentIndex());
	}
	
	public Segment nextCurveSegment(CarPosition carPosition)
	{
		Segment seg = m_Race.track().getSegment(carPosition.segmentIndex());
		
		seg = seg.next();
		while(seg.type() != SegmentType.CURVE) {
			seg = seg.next();
		}
		
		return seg;
	}
	
	public double getDistanceTo(CarPosition carPosition, Segment segment) {
		
		int laneIdx = carPosition.endLaneIndex();
		double distance = 0.0;
		
		Segment seg = m_Race.track().getSegment(carPosition.segmentIndex());
		
		//System.out.println("Segment = "+seg.id()+" ;; laneLength = "+seg.laneLength(laneIdx)+ " ;; inSegDis = "+carPosition.inSegmentDistance());
		distance += seg.laneLength(laneIdx) - carPosition.inSegmentDistance();
		seg = seg.next();	
		
		while(seg.id() != segment.id()) {
			distance += seg.laneLength(laneIdx);
			seg = seg.next();
		}
		
		if(distance < 0 )
			distance = 0;
		
		return distance;	
	}
	
	public String shortestPossibleRouteToNextSwitch(Segment segment, int currentLane) {
		if( !segment.isSwitch() ) {
			return STRAIGHT;
		}
		
		int laneCount = segment.laneCount();
		
		int leftIndex = currentLane - 1;
		int centerIndex = currentLane;
		int rightIndex = currentLane + 1;
		
		double lengthLeft = leftIndex > -1 ? 0.0 : Integer.MAX_VALUE;
		double lengthCenter = 0.0;
		double lengthRight = rightIndex <  laneCount ? 0.0 : Integer.MAX_VALUE;
		
		segment = segment.next();
		while( !segment.isSwitch() ) {
		
			if( leftIndex > -1 ) {
				lengthLeft += segment.laneLength(leftIndex);
			}
			
			if( rightIndex <  laneCount ) {
				lengthRight += segment.laneLength(rightIndex);
			}
		
			lengthCenter += segment.laneLength(centerIndex);
			
			segment = segment.next();
		}
		
		if( lengthLeft < lengthCenter ) {
			if( lengthLeft < lengthRight ) {
				return LEFT;
			} else {
				return RIGHT;
			}
		} else {
			if( lengthRight < lengthCenter ) {
				return RIGHT;
			} else {
				return STRAIGHT;
			}
		}
	}
	
//	public void determineMaxSpeed(int tick) {
//		setThrottle(0.2, tick);
//		double speedDifference = Math.abs(car().speed()-m_LastSpeed);
//		m_LastSpeed = car().speed();
//		
//		if(speedDifference < 0.001) {
//			m_MaxSpeed = Math.rint(car().speed()*5);
//		}
//	}
	
	public abstract void turboAvailable(double turboDurationMilliseconds,int turboDurationTicks, double turboFactor);
	
	public abstract void tick(int tick);
	
	public abstract void crash(int tick);
	
	public abstract void spawn(int tick);
	
	public abstract void carCrash(String color, int tick);
	
	public abstract void carSpawn(String color, int tick);
	
	public abstract void carDNF( String color, String reason, int tick );
	
	public abstract void gameInit();
	
	public abstract void gameStart();
	
	public abstract void gameEnd();
	
	public abstract void gameFinished();
	
	public abstract void lapFinished(int lapTime, int tick);
	
	public abstract void turboEnd(int tick);
	
	public abstract void turboStart( int tick );
}
