package kitt;

import java.io.IOException;

import com.google.gson.JsonSyntaxException;

import kitt.race.Race;

public class Main {
    public static void main(String... args) {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        Race race = new Race();
        race.connect(host, port);
        
        Bot bot;
        if( args.length >= 5 && args[4].equals("Kitt") ) {
        	bot = new Kitt(botName, botKey, race);
        } else {
        	bot = new Orion(botName, botKey, race);
        }
        
        if( args.length == 6 ) {
        	race.joinRace(bot, args[5], 1, null);
        } else {
        	race.join(bot);
        }
        
        while(true){
	        try {
				race.communication().receiveData();
			} catch (JsonSyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        
    }

}