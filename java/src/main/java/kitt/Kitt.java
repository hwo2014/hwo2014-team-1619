package kitt;

import kitt.race.Car;
import kitt.race.Race;
import kitt.race.car.CarPosition;
import kitt.race.track.Segment;
import kitt.race.track.Segment.SegmentType;

public class Kitt extends Bot {
	private enum PHASE {DETERMINE,QUALIFYING,RACE};
	
	private PHASE m_Phase = PHASE.DETERMINE;
	
	private double m_Throttle = 0.0;
	private boolean m_SwitchSend = false;	
	
	private boolean m_TurboAktiv = false;
	private int m_TurboStartIdx = 0;
//	private int m_TurboIndex = 0;
	
	public Kitt(String botName, String botKey, Race race) {
		super(botName, botKey, race);
	}
	
	private int initState = 0;
	private boolean initComplete = false;
	private double initMassSpeed = 0.0;
	private double initStartSpeed = 0.0;
			
	public boolean initPhase(int tick) {
		if(!initComplete) {
			switch(initState) {
			case 0:
				if(!race().gameStarted()) {
					m_Throttle = 0.0;
					sendPing(tick);
				} else {
					m_Throttle = 1.0;
					initStartSpeed = car().speed();
					++initState;
				}
				break;

			case 1:
				initMassSpeed = car().speed();
				++initState;
				break;

			case 2:
				double k = Car.calculateK(m_Throttle, initStartSpeed, initMassSpeed, car().speed());
				double mass = Car.calculateMass(1, initMassSpeed, m_Throttle, car().speed(), k);
				Car.setK(k);
				Car.setMass(mass);
				System.out.format("Mass: %10.5f, K: %10.5f%n",Car.mass(), Car.k());
				initComplete = true;
				break;

			default:
				break;
			}
		}
		return initComplete;
	}
		
	private void fastestPossibleTime(int tick) {
		CarPosition carPos = car().position();
		int lane = carPos.endLaneIndex(); //FIXME: LANE AT 
		updateTurboStatus(tick);
		 
		Segment current = currentSegment();
		Segment nextSegment = current.next();

		if(nextSegment.isSwitch() && !m_SwitchSend) {
			String nextLane = shortestPossibleRouteToNextSwitch(nextSegment,lane);
			switchLane(nextLane, tick);
			m_SwitchSend = true;
		}
		
		if(current.isSwitch()) {
			m_SwitchSend = false;
		}
		
		int tickToStop = Car.ticksToSpeed(1.0, car().speed(), 0.0);
		double distanceToMostDistantBrakingPoint = Car.positionAt(tickToStop, car().speed(), 0.0, 0);
		
		for(Segment seg = current.next(); getDistanceTo(carPos, seg)<distanceToMostDistantBrakingPoint ; seg = seg.next()) {
			double distanceToSeg = getDistanceTo(carPos, seg);
			
			int ticksTo = Car.ticksToPosition(car().speed(), 0.0, distanceToSeg);
			double speed = Car.speedAt(ticksTo, car().speed(), 0.0);
				
			if(speed > (Car.maxSpeed(seg,lane)-0.5)) { //FIXME 
				m_Throttle = 0.0;
				break;
			} else
				m_Throttle = 1.0;
		}

			
		if(Car.maxSpeed(current,lane) < car().speed())
			m_Throttle = 0.0;
			
//		if((m_Turbo && ((m_TurboIndex == current.id()) && car().position().angle() < 30)|| (m_TurboIndex+1) == current.id())) { 
//			requestTurbo("a");
//			m_Turbo = false;
//			m_TurboAktiv = true;	
//		}	
//		
//		if(m_TurboAktiv) //FIXME
//			m_Throttle = 1.0;
	}
	
	@Override
	public void tick(int tick) {
		if(m_Phase == PHASE.DETERMINE) {
			if(initPhase(tick)) {
				m_MaxSpeed = Car.maxSpeed(1.0);
				m_Phase = PHASE.QUALIFYING;
			}
		}
		
		if(m_Phase == PHASE.QUALIFYING) {	
			fastestPossibleTime(tick);
		}
		
		if(m_Phase == PHASE.RACE) {	
			fastestPossibleTime(tick);
		}
			
		if(tick%100 == 0) {
			System.out.println("TICK --------------->"+tick+"");
		}
		
		setThrottle(m_Throttle, tick);
		m_LastSpeed = car().speed();
	}
	
	@Override
	public void crash(int tick) {
	}

	@Override
	public void spawn(int tick) {
		setThrottle(1.0, tick);
	}

	@Override
	public void gameInit() {
//		double maxDistance = 0;
//		double distance = 0;
//		int maxIdx = -1;
//		int idx = -1;
//		
//		for(Segment s = nextCurveSegment(car().position()).next(); 
//					s != nextCurveSegment(car().position()); 
//					s = s.next()) {
//			
//			if(s.type() == SegmentType.CURVE && s.next().type() == SegmentType.STRAIGHT) {
//				idx = s.next().id();
//				distance = 0;
//			} else {
//				if(s.type() == SegmentType.STRAIGHT) {
//					distance += s.length();
//				}	
//			}
//
//			if(maxDistance < distance) {
//				maxDistance =  distance;
//				maxIdx = idx;
//			}
//		}
//		m_TurboIndex = maxIdx;
	}

	@Override
	public void gameStart() {
		sendPing(0);
	}

	@Override
	public void gameEnd() {
		m_TurboAktiv = false;
		
		m_Phase = PHASE.RACE;
	}

	@Override
	public void gameFinished() {
	}
	
	@Override
	public void lapFinished(int lapTime, int tick) {
		System.out.println("Lap time: " + lapTime);
	}

	@Override
	public void carCrash(String color, int tick) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void carSpawn(String color, int tick) {
		// TODO Auto-generated method stub
	}
	
	public void updateTurboStatus(int tick) {
		if(m_TurboAktiv) {
			if(tick > (m_TurboStartIdx+race().turboDurationTicks())){
				m_TurboAktiv = false;
			}
		}	
	}

	@Override
	public void carDNF(String color, String reason, int tick) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turboAvailable(double turboDurationMilliseconds,
			int turboDurationTicks, double turboFactor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turboEnd(int tick) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turboStart(int tick) {
		// TODO Auto-generated method stub
		
	}
}
