package kitt.race;

import java.util.Collection;
import java.util.List;

import kitt.race.track.Segment;

public class Track {
	
	private List<Segment> m_Segments = null;
	private String m_Name;
	private double m_Length = 0.0;
	
	public Track(List<Segment> segments) {
		m_Segments = segments;
		for( Segment segment : segments ) {
			m_Length += segment.length();
		}
	}

	public String name() {
		return m_Name;
	}
	
	public double length() {
		return m_Length;
	}
	
	public int segmentCount() {
		return m_Segments.size();
	}
	
	public Collection<Segment> segments() {
		return m_Segments;
	}
	
	public Segment getSegment(int id) {
		return m_Segments.get(id);
	}

	public Segment firstSegment() {
		return m_Segments.get(0);
	}
	
}
