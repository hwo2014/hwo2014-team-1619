package kitt.race.track;

public class CurveSegment extends Segment {

	private double m_Radius;
	
	public CurveSegment(int id,  boolean bridge, double angle, double[] laneDists, double radius, boolean isSwitch) {
		super(SegmentType.CURVE, id, 2 * radius * Math.PI * Math.abs(angle)/360.0, bridge, angle, laneDists, isSwitch);
		m_Radius = radius;
	}

	@Override
	public double laneLength(int lane) {
		return Math.PI * 2 * radius(lane) * Math.abs(angle())/360.0;
	}
	
	public double radius() {
		return m_Radius;
	}
	
	public double radius( int lane ) {
		return m_Radius - (angle() < 0.0 ? distFromCenter(lane) * - 1 : distFromCenter(lane)); //Reverse distance if we have a negative angle
	}
	
	public double phi(int lane) {
		return laneLength(lane) / Math.abs(angle());
	}
	
	public double gamma(int lane) {
		return Math.abs(radius( lane )) / laneLength(lane);
	}
	
}
