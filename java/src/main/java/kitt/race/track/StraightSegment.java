package kitt.race.track;

public class StraightSegment extends Segment {

	public StraightSegment(int id, double length, boolean bridge, double[] laneDists, boolean isSwitch) {
		super(SegmentType.STRAIGHT, id, length, bridge, 0.0, laneDists, isSwitch);
	}

	@Override
	public double laneLength(int lane) {
		return length();
	}
	
}
