package kitt.race.track;

public abstract class Segment {
	
	public enum SegmentType { CURVE, STRAIGHT };
	
	protected int m_Id = -1;
	protected Segment m_Next = null;
	protected Segment m_Prev = null;
	protected boolean m_Bridge = false;
	protected double m_Length;
	protected double m_Angle;
	protected final SegmentType m_Type;
	protected double[] m_LaneDist;
	protected boolean m_Switch;
	
	public Segment(SegmentType type, int id, double length, boolean bridge, double angle, double[] laneDists, boolean isSwitch) {
		m_Type = type;
		m_Id = id;
		m_Bridge = bridge;
		m_Length = length;
		m_Angle = angle;
		m_LaneDist = laneDists;
		m_Switch = isSwitch;
	}
	
	public void setNext( Segment next ) {
		m_Next = next;
	}
	
	public void setPrev( Segment prev ) {
		m_Prev = prev;
	}
	
	public abstract double laneLength( int lane );
	
	public boolean isSwitch() {
		return m_Switch;
	}
	
	public double distFromCenter( int lane ) {
		return m_LaneDist[lane];
	}
	
	public int laneCount() {
		return m_LaneDist.length;
	}
	
	public SegmentType type() {
		return m_Type;
	}
	
	public double angle() {
		return m_Angle;
	}
	
	public double length() {
		return m_Length;
	}
	
	public Segment next() {
		return m_Next;
	}
	
	public Segment prev() {
		return m_Prev;
	}
	
	public int id() {
		return m_Id;
	}
	
	public boolean isBridge() {
		return m_Bridge;
	}
	
}
