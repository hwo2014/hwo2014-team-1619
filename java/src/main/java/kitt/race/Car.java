package kitt.race;

import kitt.race.car.CarPosition;
import kitt.race.track.CurveSegment;
import kitt.race.track.Segment;
import kitt.race.track.Segment.SegmentType;

public class Car {
	
	private String m_Name;
	private String m_Color;
	
	private double m_Length;
	private double m_Width;
	private double m_GuideFlagPosition;
	
	private CarPosition m_CarPosition = null;
	
	private Track m_Track = null;
	
	private double m_Speed = 0.0;
	private double m_Acceleration = 0.0;
	private double m_SideAcceleration = 0.0;
	private double m_AngularVelocity = 0.0;
	private double m_AngularAcceleration = 0.0;
	private double m_LastSideAcceleration = 0.0; //For determening max curve speed
	private double m_LastAngle = 0.0;
	
	private boolean m_OnTrack = true;
	
	private static double m_MaxSideAcceleration = 0.45;
	
	private static double m_Mass = 4.95;
	private static double m_K = 0.1; //Friction coefficient
	
	private static double m_AccelerationC = 0.0;
	private static double m_SpringC = 0.0;
	private static double m_DampingC = 0.0;
	private static double m_MinSideAcceleration = 0.0;
	
	private boolean m_Turbo = false;
	
	public Car( String name, String color, double length, double width, double guideFlagPosition, Track track ) {
		m_Name = name;
		m_Color = color;
		
		m_Length = length;
		m_Width = width;
		m_GuideFlagPosition = guideFlagPosition;
		
		m_Track = track;
	}
	
	public Track track() {
		return m_Track;
	}
	
	public void updatePosition( CarPosition position) {
		if(m_CarPosition == null) {
			m_CarPosition = position;
			return;
		}
		
		Segment lastSegment = track().getSegment( m_CarPosition.segmentIndex() );
		Segment currentSegment = track().getSegment( position.segmentIndex() );
		
		double speed = 0.0;
		if( m_CarPosition.segmentIndex() == position.segmentIndex() ) {
			speed = position.inSegmentDistance() - m_CarPosition.inSegmentDistance();
		} else if(position.segmentIndex() - m_CarPosition.segmentIndex() == 1) {
			//FIXME: Real speed after switch and lane change
			if( lastSegment.isSwitch() && !currentSegment.isSwitch() && m_CarPosition.lane() == -1) {
//				System.out.println("Using old speed");
			} else {
				double segmentLength = lastSegment.laneLength(m_CarPosition.endLaneIndex());
				speed = position.inSegmentDistance() + segmentLength - m_CarPosition.inSegmentDistance();
			}
		} else {
			Segment seg = track().getSegment(m_CarPosition.segmentIndex());
			Segment endSeg = track().getSegment(position.segmentIndex());
			speed = - m_CarPosition.inSegmentDistance();
			while( seg !=  endSeg ) {
				speed += seg.laneLength(m_CarPosition.endLaneIndex());
				seg = seg.next();
			}
			speed += position.inSegmentDistance();
		}
		
		int tickDiff = position.tick() - m_CarPosition.tick();
		
		
		speed /= tickDiff < 1 ? 1 : tickDiff;
		m_Acceleration = speed - m_Speed;
		m_Speed = speed;
		
		m_LastSideAcceleration = m_SideAcceleration;
		m_SideAcceleration = 0.0;
		if( currentSegment.type() == SegmentType.CURVE) {
			
			CurveSegment c = (CurveSegment) currentSegment;
			m_SideAcceleration = sideAcceleration(m_Speed,c.radius(position.endLaneIndex()));
			if( c.angle() < 0.0 ) {
				m_SideAcceleration *= -1;
			}
		}
		
		double angularVelocity = position.angle() - m_CarPosition.angle();
		
		m_AngularAcceleration = m_AngularVelocity - angularVelocity;
		
		m_AngularVelocity = angularVelocity;
		
		m_LastAngle = m_CarPosition.angle();
				
		m_CarPosition = position;
	}
	
	public double lastAngle() {
		return m_LastAngle;
	}
	
	public static double maxSpeed( Segment segment, int lane ) {
		double maxV = maxSpeed(1.0);
		if( segment.type() == SegmentType.CURVE ) {
			CurveSegment curve = (CurveSegment) segment;
			double maxCurveSpeed = Math.sqrt(Math.abs(m_MaxSideAcceleration)*curve.radius(lane));
			maxV = maxV < maxCurveSpeed ? maxV : maxCurveSpeed;
		}
		return maxV;
	}
	
	public static double maxSpeed( double throttle ) {
		return throttle/m_K;
	}
	
	public static void setK( double k ) {
		m_K = k;
	}
	
	public static void setMass( double mass ) {
		m_Mass = mass;
	}
	
	public static double k() {
		return m_K;
	}
	
	public static double mass() {
		return m_Mass;
	}
	
	public double calculateAngle(int ticks, double angle, double angularVelocity, double initialVelocity, double throttle, CarPosition position) {
		double cAngle = angle;
		double cVelocity = angularVelocity;
		double pos = position.inSegmentDistance();
		Segment currentSegment = track().getSegment(position.segmentIndex());
		int laneIndex = position.endLaneIndex();
		for( int i = 0; i < ticks; ++i ) {
			double speed = speedAt(ticks, initialVelocity, throttle);
			pos += positionAt(1,speed,throttle,0.0);
			if( pos > currentSegment.laneLength(laneIndex) ) {
				pos -= currentSegment.laneLength(laneIndex);
				currentSegment = currentSegment.next();
			}
			double sideAcceleration = 0.0;
			if( currentSegment.type() == SegmentType.CURVE ) {
				CurveSegment curve = (CurveSegment) currentSegment;
				sideAcceleration = sideAcceleration(speed,curve.radius(laneIndex));
			}
			double cAcceleration = - angle * m_SpringC - cVelocity * m_DampingC ;
			if( Math.abs(sideAcceleration) >= Math.abs(getMinSideAcceleration() )) {
				sideAcceleration += sideAcceleration < 0.0 ? getMinSideAcceleration() : -getMinSideAcceleration();
				cAcceleration += sideAcceleration * m_AccelerationC;
			}
			cVelocity += cAcceleration;
			cAngle += cVelocity;
		}
		
		return cAngle;
	}
	
	public static double calculateK( double throttle, double startSpeed, double unaffectedSpeed, double affectedSpeed ) {
		return ((unaffectedSpeed - startSpeed) - (affectedSpeed - unaffectedSpeed))/Math.pow(unaffectedSpeed,2.0)*throttle;
	}
	
	public static double calculateMass( int tick, double initialVelocity, double throttle, double endSpeed, double k ) {
		return 1.0/(Math.log((endSpeed - (throttle/k))/(initialVelocity - (throttle/k)))/(-k*tick));
	}
	
	/*
	 * Be carefull if speed is to close to speed achievable with a given throttle, this will yield infinite ticks.
	 */
	public static int ticksToSpeed(double speed, double initialVelocity, double throttle) { 
		return (int) Math.ceil((Math.log((speed - maxSpeed(throttle))/(initialVelocity - maxSpeed(throttle)))*m_Mass)/-m_K);
	}
	
	public static double speedAt( int tick, double initialVelocity, double throttle ) {
		return (initialVelocity - maxSpeed(throttle))*Math.exp((-m_K*tick)/m_Mass) + maxSpeed(throttle);
	}
	
	public static double getThrottle(int tick, double initialVelocity, double speed) {
		return (m_K*(speed*Math.exp((m_K*tick)/m_Mass)-initialVelocity)/(Math.exp((m_K*tick)/m_Mass)-1.0));
	}
	
	public static double positionAt(int tick, double initialVelocity, double throttle, double initialPosition ) {
		return (m_Mass/m_K)*(initialVelocity - maxSpeed(throttle))*(1.0 - Math.exp((-m_K*tick)/m_Mass)) + maxSpeed(throttle) * tick + initialPosition;
	}
	
	public static int ticksToPosition( double initialVelocity, double throttle, double distance ) {
		int tick = 0;
		while( distance < positionAt(tick, initialVelocity, throttle, 0.0) ) {
			++tick;
		}
		return tick;
	}
	
	public double angularVelocity() {
		return m_AngularVelocity;
	}
	
	public double angularAcceleration() {
		return m_AngularAcceleration;
	}
	
	public double angle() {
		return m_CarPosition.angle();
	}
	
	public double speed() {
		return m_Speed;
	}
	
	public double acceleration() {
		return m_Acceleration;
	}
	
	public double sideAcceleration() {
		return m_SideAcceleration;
	}
	
	public double lastSideAcceleration() {
		return m_LastSideAcceleration;
	}
	
	public static double sideAcceleration( double speed, double radius ) {
		return Math.pow(speed, 2.0)/radius;
	}
	
	public CarPosition position() {
		return m_CarPosition;
	}
	
	public String name() {
		return m_Name;
	}
	
	public String color() {
		return m_Color;
	}
	
	public double length() {
		return m_Length;
	}
	
	public double width() {
		return m_Width;
	}
	
	public double guideFlagPosition() {
		return m_GuideFlagPosition;
	}
	
	protected void setOnTrack(boolean onTrack) {
		m_OnTrack = onTrack;
	}
	
	public boolean onTrack() {
		return m_OnTrack;
	}

	public static double accelerationC() {
		return m_AccelerationC;
	}

	public static void setAccelerationC(double accelerationC) {
		m_AccelerationC = accelerationC;
	}
	
	public static double dampingC() {
		return m_AccelerationC;
	}

	public static void setDampingC(double dampingC) {
		m_DampingC = dampingC;
	}
	
	public static double springC() {
		return m_AccelerationC;
	}

	public static void setSpringC(double springC) {
		m_SpringC = springC;
	}

	public static double getMinSideAcceleration() {
		return m_MinSideAcceleration;
	}

	public static void setMinSideAcceleration(double minSideAcceleration) {
		m_MinSideAcceleration = minSideAcceleration;
	}

	public static double getMaxSideAcceleration() {
		return m_MaxSideAcceleration;
	}

	public static void setMaxSideAcceleration(double maxSideAcceleration) {
		m_MaxSideAcceleration = maxSideAcceleration;
	}
	
	public boolean onTurbo() {
		return m_Turbo;
	}
	
	public void setOnTurbo(boolean turbo) {
		m_Turbo = turbo;
	}
}
