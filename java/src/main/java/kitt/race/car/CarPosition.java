package kitt.race.car;


public class CarPosition {

	private double m_Angle;
	private double m_InSegmentDistance;
	private int m_SegmentIndex;
	private int m_Lap;
	private int m_StartLaneIndex;
	private int m_EndLaneIndex;
	private int m_Tick;
	
	public CarPosition(int tick, double angle, double inSegmentDistance, int segmentIndex, int lap, int startLaneIndex, int endLaneIndex ) {
		m_Tick = tick;
		m_Angle = angle;
		m_InSegmentDistance = inSegmentDistance;
		m_SegmentIndex = segmentIndex;
		m_Lap = lap;
		m_StartLaneIndex = startLaneIndex;
		m_EndLaneIndex = endLaneIndex;
	}
	
	public int tick() {
		return m_Tick;
	}
	
	public double angle() {
		return m_Angle;
	}
	
	public double inSegmentDistance() {
		return m_InSegmentDistance;
	}
	
	public int segmentIndex() {
		return m_SegmentIndex;
	}
	
	public int lap() {
		return m_Lap;
	}
	
	public int startLaneIndex() {
		return m_StartLaneIndex;
	}
	
	public int endLaneIndex() {
		return m_EndLaneIndex;
	}
	
	/*
	 * @return -1 If is switching lanes
	 */
	public int lane() {
		return !isSwitching() ? m_StartLaneIndex : -1;
	}
	
	public boolean isSwitching() {
		return m_StartLaneIndex != m_EndLaneIndex;
	}
	
}
