package kitt.race;

import java.util.Collection;
import java.util.HashMap;

import kitt.Bot;
import kitt.communication.Communication;
import kitt.communication.RaceDataReceiver;
import kitt.race.car.CarPosition;

public class Race implements RaceDataReceiver {
	
	private Communication m_Communication = null;
	
	private HashMap<String,Car> m_Cars; //map containing all cars in the race. (Key: car color)
	private Track m_Track = null;
	
	private int m_Laps;
	private int m_MaxLapTimeMs;
	private boolean m_QuickRace;
	private int m_QualifyingDuration;
	private boolean m_Qualifying;
	
	private Bot m_Bot = null;
	
	private boolean m_GameStarted;
	
	private boolean m_TurboAvailable = false;
	private double m_TurboDurationMilliseconds = 0.0;
	private int m_TurboDurationTicks = 0;
	private double m_TurboFactor = 1.0;
	
	public Race() {
		m_Communication = new Communication();
		m_Communication.addDataReceiver(this);
		m_Cars = new HashMap<String,Car>();
		
		m_GameStarted = false;
	}
	
	public Communication communication() {
		return m_Communication;
	}
	
	public Track track() {
		return m_Track;
	}
	
	public int carCount() {
		return m_Cars.size();
	}
	
	public Car getCar(String color) {
		return m_Cars.get(color);
	}
	
	public Collection<Car> getCars() {
		return m_Cars.values();
	}
	
	public boolean connect(String host, int port) {
		if(!m_Communication.connect(host, port)) {
			return false;
		}
		return true;
	}
	
	public boolean join( Bot bot ) {
		if( m_Bot != null ) {
			return false;
		}

		if( !m_Communication.join(bot) ) {
			return false;
		}
		
		m_Bot = bot;
		
		return true;
	}
	
	public boolean joinRace( Bot bot, String trackName, int carCount, String password ) {
		if( m_Bot != null ) {
			return false;
		}

		if( !m_Communication.joinRace(bot, trackName, carCount, password) ) {
			return false;
		}
		
		m_Bot = bot;
		
		return true;
	}
	
	public boolean createRace( Bot bot, String trackName, int carCount, String password ) {
		if( m_Bot != null ) {
			return false;
		}

		if( !m_Communication.createRace(bot, trackName, carCount, password) ) {
			return false;
		}
		
		m_Bot = bot;
		
		return true;
	}
	
	public int laps() {
		return m_Laps;
	}
	
	public int maxLapTimeMs() {
		return m_MaxLapTimeMs;
	}
	
	public boolean isQuickRace() {
		return m_QuickRace;
	}
	
	public boolean isQualifying() {
		return m_Qualifying;
	}
	
	public int qualifyingDuration() {
		return m_QualifyingDuration;
	}
	
	public boolean gameStarted() {
		return m_GameStarted;
	}
	
	@Override
	public void receivedGameJoin( String name, String key ) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void receivedGameStart() {
		m_GameStarted = true;
		m_Bot.gameStart();
	}

	@Override
	public void receivedOwnCar(String name, String color) {
		m_Bot.setColor(color);
	}

	@Override
	public void receivedGameInit(Track track, Collection<Car> cars, int laps, int maxLapTimeMs, boolean quickRace, int durationMs ) {
		m_Track = track;
		
		m_Cars.clear();
		for( Car car : cars ) {
			m_Cars.put(car.color(), car);
		}
		
		m_Laps = laps;
		m_MaxLapTimeMs = maxLapTimeMs;
		m_QuickRace = quickRace;
		m_QualifyingDuration = durationMs;
		m_Qualifying = durationMs > 0;
		
		m_Bot.setCar(m_Cars.get(m_Bot.color()));
		
		m_Bot.gameInit();
	}

	@Override
	public void receivedCarPositions(HashMap<String, CarPosition> positions, int tick) {
		for( String color : positions.keySet() ) {
			Car car = m_Cars.get(color);
			if( car != null ) {
				car.updatePosition(positions.get(color));
			}
		}
		
		m_Bot.tick(tick);
	}

	@Override
	public void receivedCrash(String name, String color, int tick) {
		m_Cars.get(color).setOnTrack(false);
		
		if( color.equals(m_Bot.color()) ) {
			m_Bot.crash(tick);
		}
		
		m_Bot.carCrash(color, tick);
	}

	@Override
	public void receivedSpawn(String name, String color, int tick) {
		m_Cars.get(color).setOnTrack(true);
		
		if( color.equals(m_Bot.color()) ) {
			m_Bot.spawn(tick);
		}
		
		m_Bot.carSpawn(color, tick);
	}

	@Override
	public void receivedFinished(String gameId, String name, String color) {
		m_Cars.get(color).setOnTrack(false); //TODO: Test if car disappears
		
		if( color.equals(m_Bot.color()) ) {
			m_Bot.gameFinished();
		}
	}

	@Override
	public void receivedTournamentEnd() {
		communication().disconnect();
		System.exit(0); //FIXME: PRoper termination
	}

	@Override
	public void receivedGameEnd() {
		m_GameStarted = false;
		m_TurboAvailable = false;
		m_TurboDurationMilliseconds = 0.0;
		m_TurboDurationTicks = 0;
		m_TurboFactor = 1.0;
		m_Bot.gameEnd();
	}

	@Override
	public void receivedLapFinished(String color, int lapTime, int tick) {
		if( color.equals(m_Bot.color()) ) {
			m_Bot.lapFinished(lapTime, tick);
		}
	}

	@Override
	public void receivedTurboAvailable(double turboDurationMilliseconds,
			int turboDurationTicks, double turboFactor) {
		
		m_TurboAvailable = true;
		m_TurboDurationMilliseconds = turboDurationMilliseconds;
		m_TurboDurationTicks = turboDurationTicks;
		m_TurboFactor = turboFactor;
		m_Bot.turboAvailable(turboDurationMilliseconds,turboDurationTicks,turboFactor);
	}
	
	public boolean turboAvailable() {
		return m_TurboAvailable;
	}
	
	public double turboDurationMilliseconds() {
		return m_TurboDurationMilliseconds;
	}
	
	public int turboDurationTicks() {
		return m_TurboDurationTicks;
	}
	
	public double turboFactor() {
		return m_TurboFactor;
	}
	
	public void requestTurbo(String turboMessage) {
		if( m_TurboAvailable ) {
			m_Communication.sendTurbo(turboMessage);
			m_TurboAvailable = false;
			m_TurboDurationMilliseconds = 0.0;
			m_TurboDurationTicks = 0;
			m_TurboFactor = 1.0;
		}
	}

	@Override
	public void receivedDNF(String name, String color, String reason, int tick) {
		m_Cars.get(color).setOnTrack(false);
		m_Bot.carDNF(color, reason, tick);
	}

	@Override
	public void receivedTurboStart(String name, String color, int tick) {
		m_Cars.get(color).setOnTurbo(true);
		if( color.equals(m_Bot.color()) ) {
			m_Bot.turboStart(tick);
		}
	}

	@Override
	public void receivedTurboEnd(String name, String color, int tick) {
		m_Cars.get(color).setOnTurbo(false);
		if( color.equals(m_Bot.color()) ) {
			m_Bot.turboEnd(tick);
		}
	}

}
