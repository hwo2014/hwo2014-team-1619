package kitt.regulator;

public interface Regulator {
	public double getThrottle(double desiredSpeed, double currentSpeed, int tick);
	public void reset(int tick);
}
