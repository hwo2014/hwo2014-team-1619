package kitt.regulator;

public class PI implements Regulator{
	private static final double EPS = 0.000001;
	private double m_PI_P = 0.6;
	private double m_PI_I = 0.02;
	private int m_PI_LastTick = 0;
	private double m_PI_Integral = 0.0;
	private double m_PI_Output = 0.0;
	
	public double getThrottle(double desiredSpeed, double currentSpeed, int tick) {
		double dt = tick - m_PI_LastTick;
		
		double error = desiredSpeed - currentSpeed;
		if( m_PI_Output >= 0.0 && m_PI_Output < 1.0 ) {
			m_PI_Integral = m_PI_Integral + error * dt;
		}
		
		m_PI_LastTick = tick;
		
		m_PI_Output = m_PI_P * error + m_PI_I * m_PI_Integral;
		
		if(m_PI_Output < EPS) {
			m_PI_Output = 0.0;
		} else if( m_PI_Output > 1.0 ) {
			m_PI_Output = 1.0;
		}
		return m_PI_Output;
	}
	
	public void reset(int tick) {
		m_PI_Integral = 0.0;
		m_PI_LastTick = tick;
		m_PI_Output = 0.0;
	}
}
