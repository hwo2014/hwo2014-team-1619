package kitt;

import java.util.Locale;

import kitt.race.Car;
import kitt.race.Race;
import kitt.race.track.CurveSegment;
import kitt.race.track.Segment;
import kitt.race.track.Segment.SegmentType;

public class Orion extends Bot {

	private int initState = 0;
	private boolean initComplete = false;
	private double initMassSpeed = 0.0;
	private double initStartSpeed = 0.0;
	public boolean init(int tick) {
		if(!initComplete) {
			switch(initState) {
			case 0:
				if(!race().gameStarted()) {
					m_Throttle = 0.0;
					sendPing(tick);
				} else {
					m_Throttle = 1.0;
					initStartSpeed = car().speed();
					++initState;
				}
				break;

			case 1:
				initMassSpeed = car().speed();
				++initState;
				break;

			case 2:
				double k = Car.calculateK(m_Throttle, initStartSpeed, initMassSpeed, car().speed());
				double mass = Car.calculateMass(1, initMassSpeed, m_Throttle, car().speed(), k);
				Car.setK(k);
				Car.setMass(mass);
				System.out.format("Mass: %10.5f, K: %10.5f%n",Car.mass(), Car.k());
				initComplete = true;
				break;

			default:
				break;

			}
		}

		return !initComplete;
	}

	private boolean angleInitComplete = false;
	private int angleInitState = 0;
	private int samples = 20;
	private int curveSamples = 3;
	private int samplesCollected = 0;
	private double[] angle = new double[samples];
	private double[] sideAcceleration = new double[samples];
	private double[] angleVel = new double[samples];
	private double[] angleAccel = new double[samples];
	private double[] carSpeed = new double[samples];
	private double[] curveRadius = new double[samples];
	public boolean angleInit(int tick) {
		if(!angleInitComplete) {
			switch(angleInitState) {

			case 0:
				if( car().angle() != 0.0 ) {
					++samplesCollected;
					sideAcceleration[samplesCollected] = car().sideAcceleration();
					angle[samplesCollected] = car().angle();
					carSpeed[samplesCollected] = car().speed();
					if( currentSegment().type() == SegmentType.CURVE ) {
						CurveSegment curve = (CurveSegment) currentSegment();
						curveRadius[samplesCollected] = curve.radius(car().position().endLaneIndex());
					} else {
						curveRadius[samplesCollected] = 0.0;
					}
					++angleInitState;
					++samplesCollected;
				} else {
					m_Throttle = 1.0;
					sideAcceleration[samplesCollected] = car().sideAcceleration();
					angle[samplesCollected] = car().angle();
					carSpeed[samplesCollected] = car().speed();
					if( currentSegment().type() == SegmentType.CURVE ) {
						CurveSegment curve = (CurveSegment) currentSegment();
						curveRadius[samplesCollected] = curve.radius(car().position().endLaneIndex());
					} else {
						curveRadius[samplesCollected] = 0.0;
					}
				}
				break;

			case 1:
				sideAcceleration[samplesCollected] = car().sideAcceleration();
				angle[samplesCollected] = car().angle();
				carSpeed[samplesCollected] = car().speed();
				if( currentSegment().type() == SegmentType.CURVE ) {
					CurveSegment curve = (CurveSegment) currentSegment();
					curveRadius[samplesCollected] = curve.radius(car().position().endLaneIndex());
				} else {
					curveRadius[samplesCollected] = 0.0;
				}
				++samplesCollected;
				if( samplesCollected == curveSamples ) {
					//++angleInitState;
					angleInitState = 4;
				}
				break;
				
			case 2:
				if( car().angle() != 0.0 && currentSegment().type() == SegmentType.STRAIGHT) {
					++samplesCollected;
					sideAcceleration[samplesCollected] = car().sideAcceleration();
					angle[samplesCollected] = car().angle();
					carSpeed[samplesCollected] = car().speed();
					curveRadius[samplesCollected] = 0.0;
					++angleInitState;
					++samplesCollected;
				} else if(currentSegment().type() == SegmentType.STRAIGHT) {
					sideAcceleration[samplesCollected] = car().sideAcceleration();
					angle[samplesCollected] = car().angle();
					carSpeed[samplesCollected] = car().speed();
					curveRadius[samplesCollected] = 0.0;
				}
				break;
				
			case 3:
				if(currentSegment().type() == SegmentType.STRAIGHT) {
					sideAcceleration[samplesCollected] = car().sideAcceleration();
					angle[samplesCollected] = car().angle();
					carSpeed[samplesCollected] = car().speed();
					curveRadius[samplesCollected] = 0.0;
					++samplesCollected;
					if( samplesCollected == samples ) {
						++angleInitState;
					}
				}
				break;
				
			case 4:
				angleVel[0] = 0.0;
				angleAccel[0] = 0.0;
				for( int i = 1; i < samples; ++i ) {
					angleVel[i] = angle[i]-angle[i-1];
					angleAccel[i] = angleVel[i] - angleVel[i-1];
				}
				double accelC = 0.0;
				double springC = 0.0;
				double damperC = 0.0;
				
				double[][] values = {{-angle[15],-angleVel[15]},{-angle[18],-angleVel[18]}};
				double[] result = {angleAccel[16],angleAccel[19]};
				/*RealMatrix rmValues = new Array2DRowRealMatrix(values);
		        RealVector rmResult = new ArrayRealVector(result);
		        
		        DecompositionSolver solver = new LUDecomposition(rmValues).getSolver();
		        RealVector rvSolution = solver.solve(rmResult);
		        RealVector residual = rmValues.operate(rvSolution).subtract(rmResult);
		        double rnorm = residual.getLInfNorm();
		        
		        springC = rvSolution.getEntry(0);
		        damperC = rvSolution.getEntry(1);
		        */
		        double minSideAcceleration = Math.pow(Math.toRadians(Math.toDegrees(carSpeed[0]/curveRadius[0])-angle[1])*curveRadius[0],2.0)/curveRadius[0];
		        double maxSideAcceleration = (sideAcceleration[0] - minSideAcceleration)/angle[1];
		        
		        accelC = maxSideAcceleration;//((angleAccel[8] + springC * angleAccel[7] + damperC * angleVel[7])- angleAccel[0])/(sideAcceleration[0]-sideAcceleration[7]);
		        
		        Car.setAccelerationC(accelC);
		        Car.setSpringC(springC);
		        Car.setDampingC(damperC);
		        Car.setMinSideAcceleration(Math.abs(minSideAcceleration));
		        
		        angleInitComplete = true;
				break;

			default:

				break;
			}
		}
		return !angleInitComplete;
	}

	public void printSegmentTypeOnSwitch() {
		Segment current = currentSegment();
		if( current != lastSegment ) {
			System.out.print("Segment switch ID:" + current.id());
			if( current.type() == SegmentType.STRAIGHT ) {
				System.out.println(" Type: STRAIGHT");
			} else {
				System.out.println(" Type: CURVE");
			}
		}
		lastSegment = current;
	}
	
	public double maxSpeedThrottle(int tick) {
		int currentLane = car().position().endLaneIndex();
		if(currentSegment().type() == SegmentType.CURVE && ((Car.maxSpeed(currentSegment(),currentLane) <= car().speed() || Car.maxSpeed(currentSegment(),currentLane) <= Car.speedAt(1, car().speed(), 1.0)) && Math.abs(car().sideAcceleration()) > Math.abs(Car.getMinSideAcceleration())) || Math.abs(car().position().angle()) > maxAngle) {
			return 0.0;
		}
		
		int tickToStop = Car.ticksToSpeed(0.5, car().speed(), 0.0);
		double distanceToMostDistantBrakingPoint = Car.positionAt(tickToStop, car().speed(), 0.0, 0.0);
		
		double throttle = 1.0;
		for(Segment seg = currentSegment().next(); getDistanceTo(car().position(), seg) < distanceToMostDistantBrakingPoint ; seg = seg.next()) {
			double distanceToSeg = getDistanceTo(car().position(), seg);
			
			int ticksTo = Car.ticksToPosition(car().speed(), 0.0, distanceToSeg);
			double speed = Car.speedAt(ticksTo, car().speed(), 0.0);
				
			if(seg.type() == SegmentType.CURVE) {
				CurveSegment curve = (CurveSegment) seg;
				
				double angleDiff = seg.angle() < 0.0 ? -60.0 : 60.0;
				angleDiff -= car().position().angle();
				//speed -= 2.0/100.0 * speed * Math.abs(angleDiff) / 120.0 * curve.gamma(currentLane);
				
				if( speed > (Car.maxSpeed(seg,currentLane)) ) {
					double sideAcceleration = Car.sideAcceleration(speed,curve.radius(currentLane));
					if( Math.abs(sideAcceleration) >= Math.abs(Car.getMinSideAcceleration()) ) {
						throttle = 0.0;
					} 
				}
			}
		}
		
		return throttle;
	}
	
	public boolean boost() {
		if( !car().onTurbo() && race().turboAvailable() && currentSegment().type() != SegmentType.CURVE ) {
			double turboDistance = Car.positionAt(race().turboDurationTicks(), car().speed(), race().turboFactor(), 0.0);
			int lane = car().position().endLaneIndex();
			Segment seg = currentSegment();
			double distanceToCurve = seg.laneLength(lane) - car().position().inSegmentDistance();
			seg = seg.next();
			while(seg.type() != SegmentType.CURVE ) { // || Car.speedAt(Car.ticksToPosition(car().speed(), race().turboFactor(), distanceToCurve), car().speed(), race().turboFactor()) > Car.maxSpeed(seg, lane)
				distanceToCurve += seg.laneLength(lane);
				seg = seg.next();
			}
			if( distanceToCurve >= turboDistance || currentSegment() == m_LongestStraightSegment) {
				return true;
			}
		}
		return false;
	}

	private double m_Throttle = 0.0;
	private String m_NextLane = STRAIGHT;
	private Segment m_SwitchSegment = null;
	private Segment m_LongestStraightSegment = null;
	private double maxAngle = 55.0;

	public Orion(String botName, String botKey, Race race) {
		super(botName, botKey, race);
	}

	private Segment lastSegment = null;

	@Override
	public void tick(int tick) {
		if( !init(tick) ) {
			if(!angleInit(tick)) {
				if(!race().gameStarted()) {
					return;
				}
				Segment current = currentSegment();
				Segment nextSegment = current.next();

				printSegmentTypeOnSwitch();
				
				if( boost() ) {
					m_Throttle = 1.0;
					requestTurbo("Go Go");
				}
				
				m_Throttle = maxSpeedThrottle(tick);
				if( m_SwitchSegment != nextSegment ) {
					m_NextLane = shortestPossibleRouteToNextSwitch(nextSegment,car().position().lane());
				} else {
					m_NextLane = STRAIGHT;
				}
				
				//int futureTicks = 20;
				//double angle = car().calculateAngle(futureTicks, car().angle(), car().angularVelocity(), car().speed(), m_Throttle, car().position());
				
				//System.out.println("Predicted angle: " + angle + " for Tick " +  (tick+futureTicks) +  " actual angle: " + car().angle() +  " current tick " + tick);

				//System.out.format("%10.5f %10.5f%n",car().speed(), Car.speedAt(tick-1,0.0,1.0));
			}
		}
		if( currentSegment().next().isSwitch() && m_SwitchSegment != currentSegment().next()) {
			switchLane( m_NextLane, tick);
			m_SwitchSegment = currentSegment().next();
		}
		if( currentSegment() == m_SwitchSegment ) {
			m_SwitchSegment = null;
		}
		setThrottle( m_Throttle, tick );	
	}

	@Override
	public void crash(int tick) {
		if( currentSegment().type() == SegmentType.CURVE ) {
			CurveSegment c = (CurveSegment) currentSegment();
			if( Math.abs(car().lastSideAcceleration()) < Math.abs(Car.getMaxSideAcceleration()) ) {
				Car.setMaxSideAcceleration(Math.abs(car().lastSideAcceleration()));
			}
			if( Math.abs(car().lastAngle()) < Math.abs(maxAngle) ) {
				maxAngle = Math.abs(car().lastAngle());
			}
			System.out.println("Speed: " + car().speed() + " Radius: " + c.radius(car().position().lane()) + "max " + car().lastSideAcceleration());
		}
	}

	@Override
	public void spawn(int tick) {
		setThrottle( m_Throttle, tick );
	}

	@Override
	public void gameInit() {
		m_LongestStraightSegment = null;
		double longestSegmentLength = 0.0;
		System.out.println("a");
		Segment currentSegment = race().track().firstSegment();
		while( currentSegment.type() != SegmentType.CURVE && currentSegment.next() != race().track().firstSegment() ) {
			currentSegment = currentSegment.next();
		}
		Segment endSegment = currentSegment;
		currentSegment = currentSegment.next();
		while( currentSegment != endSegment ) {
			while( currentSegment.type() == SegmentType.CURVE && currentSegment != endSegment ) {
				currentSegment = currentSegment.next();
			}
			double length = currentSegment.length();
			Segment start = currentSegment;
			while( currentSegment.next().type() == SegmentType.STRAIGHT && currentSegment.next() != endSegment) {
				currentSegment = currentSegment.next();
				length += currentSegment.length();
			}
			if( length > longestSegmentLength ) {
				m_LongestStraightSegment = start;
				longestSegmentLength = length;
			}
			currentSegment = currentSegment.next();
		}
		if( m_LongestStraightSegment != null ) {
			System.out.println("Longest segment start: " + m_LongestStraightSegment.id());
		}
	}

	@Override
	public void gameStart() {
		m_Throttle = 0.0;
		m_NextLane = STRAIGHT;
		m_SwitchSegment = null;
		setThrottle( m_Throttle, 0 );
	}

	@Override
	public void gameEnd() {
	}

	@Override
	public void gameFinished() {
		// TODO Auto-generated method stub

	}

	@Override
	public void lapFinished(int lapTime, int tick) {
		System.out.println("Lap time: " + lapTime);
	}

	@Override
	public void turboAvailable(double turboDurationMilliseconds,
			int turboDurationTicks, double turboFactor) {
		// TODO Auto-generated method stub
	}

	@Override
	public void carCrash(String color, int tick) {
		// TODO Auto-generated method stub
	}

	@Override
	public void carSpawn(String color, int tick) {
		// TODO Auto-generated method stub
	}

	@Override
	public void carDNF(String color, String reason, int tick) {
		// TODO Auto-generated method stub
	}

	@Override
	public void turboEnd(int tick) {
	}

	@Override
	public void turboStart(int tick) {
	}
}
